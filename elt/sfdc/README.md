### Adding fields
To add fields, update the `sfdc_manifest.yaml` file. Note that the attribute name should be lowercased. The schema should be automatically updated on the next run.